variable "instance_count" {
  type = number
}

variable "runcmd1" {
  type = string
}

variable "runcmd2" {
  type = string
}

variable "runcmd3" {
  type = string
}

variable "loadcmd" {
  type = string
}

provider "aws" {
  access_key = "" # add key
  secret_key = "" # add key
  region     = "us-east-2"
}

resource "aws_instance" "test_master" {
  count         = var.instance_count
  ami           = "ami-06d5c50c30a35fb88"
  instance_type = "t2.medium"
  key_name      = "cca"
  vpc_security_group_ids = [
    "sg-0c8877cfa1f19cd8a"
  ]

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file("cca.pem")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "set -e",
      "sudo yum install -y telnet python37 maven",
      "sudo amazon-linux-extras install java-openjdk11 -y",
      "sudo curl -O --location https://github.com/brianfrankcooper/YCSB/releases/download/0.17.0/ycsb-0.17.0.tar.gz",
      "sudo tar xfvz ycsb-0.17.0.tar.gz",
      "cd ycsb-0.17.0",
      "${var.loadcmd}",
      "${var.runcmd1}",
      "${var.runcmd2}",
      "${var.runcmd3}"
    ]
  }
}

output "ec2_public_ip" {
  value = jsonencode([
    for instance in aws_instance.test_master : instance.public_ip
  ])
}
