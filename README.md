# Benchmarking Tool

This tool provides end-to-end automation for benchmarking using YCSB. The tool deploys instances, installs YCSB, collects results and present plots for comparision.

### Requirements
```
- Python3
- Terraform
- AWS IAM credentials
    - Access key
    - Security key
```

<img src="fts_tool_arch.png">

### Setup
1. Clone the repo:
```
git clone https://gitlab.engr.illinois.edu/santosh8/cs598-fts-project.git
```

2. Prepare directory for Terraform
```
cd cs598-fts-project
terraform init
```

3. Add AWS IAM user keys to ```main.tf``` (line 24-28)
```
provider "aws" {
  access_key = "" # add key
  secret_key = "" # add key
  region     = "us-east-2"
}
```

4. Add private key (.pem) file to the directory

### Usage:

Run the script with super user access as it should createe output files
```
usage: fts_benchmarking_tool.py [-h] --max_vms MAX_VMS --load LOAD --db DB --endpoint ENDPOINT --workload WORKLOAD [--pk PK]
                                [--tablename TABLENAME] [--region REGION]

options:
  -h, --help            show this help message and exit
  --max_vms MAX_VMS     maximum number of virtual machines
  --load LOAD           load on the system
  --db DB               name of the database
  --endpoint ENDPOINT   db endpoint
  --workload WORKLOAD   workload a or b or c
  --pk PK               primary key for dynamodb
  --tablename TABLENAME
                        table name for dynamodb
  --region REGION       region
```

### Example usage:
#### Redis:
```sudo python3 fts_benchmarking_tool.py --db redis --load 1000000 --workload a --endpoint "fts-0001-001.wnn2eh.0001.use2.cache.amazonaws.com" --max_vms 5```

#### Memcached:
```sudo python3 fts_benchmarking_tool.py --db memcached --load 1000000 --workload a --endpoint "ftsmemd.wnn2eh.0001.use2.cache.amazonaws.com" --max_vms 5```

#### DynamoDB:
```sudo python3 fts_benchmarking_tool.py --db dynamodb --load 1000000 --workload a --endpoint "http://dynamodb.us-east-1.amazonaws.com/" --max_vms 5 --tablename=usertable --region=us-east-1 --pk timestamp```